package tp.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;

import com.cloudant.client.api.CloudantClient;
import com.cloudant.client.api.Database;

import tp.model.Animal;
import tp.model.AnimalNotFoundException;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

@Path("/zoo-manager/")
public class MyServiceTP {

    private Center center = new Center(new LinkedList<>(), new Position(49.30494d, 1.2170602d), "Biotropica");
    private Database db;
    private CloudantClient client;
    
    public MyServiceTP() {
    	/*client = ClientBuilder.account("85f31d3a-f363-47fe-86b8-42c70e3205dd-bluemix")
			                .username("85f31d3a-f363-47fe-86b8-42c70e3205dd-bluemix")
			                .password("c8fa09b2e8c6a69143ee71311358dd1aef121d4e")
			                .build();*/
    	
        // Fill our center with some animals
        /*Cage usa = new Cage(
                "usa",
                new Position(49.305d, 1.2157357d),
                25,
                new LinkedList<>(Arrays.asList(
                        new Animal("Tic", "usa", "Chipmunk", UUID.randomUUID()),
                        new Animal("Tac", "usa", "Chipmunk", UUID.randomUUID())
                ))
        );

        Cage amazon = new Cage(
                "amazon",
                new Position(49.305142d, 1.2154067d),
                15,
                new LinkedList<>(Arrays.asList(
                        new Animal("Canine", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Incisive", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Molaire", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("De lait", "amazon", "Piranha", UUID.randomUUID())
                ))
        );
        center.getCages().addAll(Arrays.asList(usa, amazon));*/
        
    	client = ConnectionHelper.createClient();
    	
    	db = client.database("animalsdb", true);
    	
    	downloadCenter();
    	uploadCenter();
    }
    
    private void uploadCenter() {    
    	try {
			Map<String, String> d = db.getAllDocsRequestBuilder().includeDocs(true).build().getResponse().getIdsAndRevs();
			if (d != null) {
				d.forEach((k, v) -> db.remove(k, v));
			}
			db.save(center);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    private void downloadCenter() { 
    	try {
			center = db.getAllDocsRequestBuilder().includeDocs(true).build().getResponse().getDocsAs(Center.class).get(0);
		} catch (IndexOutOfBoundsException e) {
			//Nothing
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    /**
     * GET method bound to calls on /animals/{something}
     */
    @GET
    @Path("/animals/{id}/")
    @Produces("application/xml")
    public Animal getAnimal(@PathParam("id") String animal_id) throws JAXBException {
        try {
            return center.findAnimalById(UUID.fromString(animal_id));
        } catch (AnimalNotFoundException e) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    /**
     * GET method bound to calls on /animals in XML
     */
    @GET
    @Path("/animals/")
    @Produces("application/xml")
    public Center getAnimals(){
        return this.center;
    }
    
    /**
     * GET method bound to calls on /animals in JSON
     */
    @GET
    @Path("/animalsdb/")
    @Produces("application/json")
    public Center getAnimalsJson() {
    	return center;
    }

    /**
     * POST method bound to calls on /animals
     */
    @POST
    @Path("/animals/")
    @Consumes("application/xml")
    @Produces("application/xml")
    public void postAnimals(Animal animal) throws JAXBException {
        this.center.getCages()
                .stream()
                .filter(cage -> cage.getName().equals(animal.getCage()))
                .findFirst()
                .orElseThrow(() -> new WebApplicationException(Response.Status.NOT_FOUND))
                .getResidents()
                .add(animal);
        //Envoi du centre sur la BDD
        uploadCenter();
    }
    
    /**
     * GET Method bound to calls on /animals/{id}/wolf
     */
    @GET
    @Path("/animals/{id}/wolf")
    @Produces("application/xml")
    public String wolframInfo(@PathParam("id") String animal_id) throws JAXBException {
		try {
			//Search animal who is identified by the animal_id
			Animal animal = center.findAnimalById(UUID.fromString(animal_id));
			//Get info from wolfram
			String wolfInfo = requestHttpGetOn("https://api.wolframalpha.com/v2/query?input="+animal.getSpecies()+"&output=XML&appid=JA52GU-5GE7642L3K");
			//Return info as String 
			return wolfInfo;
        } catch (AnimalNotFoundException e) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
	}
    
    /**
     * Method who do a GET HTTP request on uri
     * @param uri
     * @return result of the request
     */
    private String requestHttpGetOn(String uri) {
		try { 
		    //Create an HttpUrlConnection (GET)
			URL url = new URL(uri);
		    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		    connection.setDoOutput(true);
		    connection.setRequestMethod("GET");
		    connection.setRequestProperty("Content-Type", "application/xml");
		    
		    //If response code is HTTP_OK (200)
		    if(connection.getResponseCode() == 200) {
		    	//Get content of the response
		        StringBuilder result = new StringBuilder();
		    	BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		        String line;
		        while ((line = rd.readLine()) != null) {
		           result.append(line);
		        }
		        rd.close();
			    connection.disconnect();
			    //Return response as String
		        return result.toString();
		    } else {
		    	//Return null if response code isn't HTTP_OK (200)
			    connection.disconnect();
		    	return null;
		    }
		} catch(Exception e) {
		    throw new RuntimeException(e);
		}
    }
    
    /**
     * POST Method bound to calls on /cage
     */
    @POST
    @Path("/cage/")
    @Consumes("application/xml")
    @Produces("application/xml")
    public void postCage(Cage c) throws JAXBException {
		//Decode source as Cage
        //Verify if cage already exist (search by name)
        for(Cage cage : center.getCages()) {
        	if(c.getName().equals(cage.getName())) {
        		//If yes => Error HTTP 409 : Conflict
        		throw new WebApplicationException(Response.Status.CONFLICT); 
        	}
        }
        //If not found => add the cage and return the cage
        this.center.getCages().add(c);
        //Envoi du centre sur la BDD
        uploadCenter();
	}
    
    /**
     * DELETE Method bound to calls on /cage/{name}
     */
    @DELETE
    @Path("/cage/{name}")
    @Produces("application/xml")
    public void deleteCage(@PathParam("name") String name) throws JAXBException {
		Cage cage = null;
		//Search the cage with his {name}
		for(Cage c : center.getCages()) {
			if(c.getName().equals(name)) {
				cage = c;
			}
		}
		//If found remove the cage
		if(cage != null) {
			center.getCages().remove(cage);
		} else {
			//If not return HTTP 404 error  
			 throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		//Envoi du centre sur la BDD
        uploadCenter();
	}
    
    /**
     * DELETE Method bound to calls on /cage/{name}/animal
     */
    @DELETE
    @Path("/cage/{name}/animal")
    @Produces("application/xml")
    public void deleteAnimalInCage(@PathParam("name") String name) throws JAXBException {
		Cage cage = null;
		//Search the cage with his {name}
		for(Cage c : center.getCages()) {
			if(c.getName().equals(name)) {
				cage = c;
			}
		}
		//If found remove all animals in the cage
		if(cage != null) {
			cage.getResidents().clear();
		} else {
			//If not return HTTP 404 error  
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		//Envoi du centre sur la BDD
        uploadCenter();
	}
    
    /**
     * GET Method bound to calls on /find/byName/{name}
     */
    @GET
    @Path("/find/byName/{name}")
    @Produces("application/xml")
    public Animal findByName(@PathParam("name") String name) throws JAXBException {
		//Search in all cage, all residents
		for( Cage c : this.center.getCages() ) {
			for( Animal a : c.getResidents() ) {
				//Return the animal who have the same name as {name}
				if( a.getName().equals(name) ) {
		    		return a;
				}
			}
		}
		//If no animal found 404 ERROR
		throw new WebApplicationException(Response.Status.NOT_FOUND);
	}
    
    /**
     * GET Method bound to calls on /find/at/{position}
     */
    @GET
    @Path("/find/at/{position}")
    @Produces("application/xml")
    public Animal findAt(@PathParam("position") String position) throws JAXBException {
		//Search in all cage who is at the {position}
		for( Cage c : this.center.getCages() ) {
			if( c.getPosition().toString().equals(position) && !c.getResidents().isEmpty()) {
				//Return the first animal of the resident list
    		    return (Animal) c.getResidents().toArray()[0];
			}
		}
		//If no animal found 404 ERROR
		throw new WebApplicationException(Response.Status.NOT_FOUND);
	}
    
    /**
     * GET Method bound to calls on /find/near/{position}
     */
    @GET
    @Path("/find/near/{position}")
    @Produces("application/xml")
    public Animal[] findNear(@PathParam("position") String position) throws JAXBException {
		//Parse the position
		String[] posStr = position.substring(1, position.length()-1).replace(" ", "").split(",");
		Double latitude = Double.parseDouble(posStr[0]);
		Double longitude = Double.parseDouble(posStr[1]);
		Position pos = new Position(latitude,longitude);
		
		//Search in all cage who is near the {position}
		Cage cagefound = this.near(pos);
		if( cagefound != null && !cagefound.getResidents().isEmpty()) {
			//Return the resident list of the cage found
			Animal[] animalsArray = new Animal[cagefound.getResidents().size()];
		    return cagefound.getResidents().toArray(animalsArray);
		}
		//If no cage found 404 ERROR
		throw new WebApplicationException(Response.Status.NOT_FOUND);
	}

    /**
     * Method who return the cage who is near to p
     * @param p
     * @return cage near to p
     */
    private Cage near(Position p) {
    	Cage cFound = null;
    	double dist = 150000;
    	//Search a cage who in near (150km) of the position p
    	for(Cage c : center.getCages()) {
    		if(getDistance(c.getPosition(),p) <= dist) {
        		cFound = c;
    		}
    	}
    	return cFound;
    }
    
    /**
     * Return the distance between position a and b
     * @param a
     * @param b
     * @return distance between position a and b
     */
    private double getDistance(Position a, Position b) {
    	//--- Begin of Hard math ---//
        float pk = (float) (180.f/Math.PI);
       
        double a1 = a.getLatitude() / pk;
        double a2 = a.getLongitude() / pk;
        double b1 = b.getLatitude() / pk;
        double b2 = b.getLongitude() / pk;
       
        double d1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double d2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double d3 = Math.sin(a1) * Math.sin(b1);
        double dist = Math.acos(d1 + d2 + d3);
        //--- End of Hard math ---//
        return (6366000 * dist);
    }
    
    /**
     * POST Method bound to calls on /animals/{animal_id}
     */
    @POST
    @Path("/animals/{animal_id}")
    @Consumes("application/xml")
    @Produces("application/xml")
    public void postAnimal(Animal animal, @PathParam("animal_id") String animal_id) throws JAXBException {
    	// Set the id of the animal which will be add 
    	animal.setId(UUID.fromString(animal_id));
        this.center.getCages()
                .stream()
                .filter(cage -> cage.getName().equals(animal.getCage()))
                .findFirst()
                .orElseThrow(() -> new WebApplicationException(Response.Status.NOT_FOUND))
                .getResidents()
                .add(animal);
        //Envoi du centre sur la BDD
        uploadCenter();
    }
    
    /**
     * DELETE Method bound to calls on /animals/{animal_id}
     */
    @DELETE
    @Path("/animals/{animal_id}")
    @Produces("application/xml")
    public void deleteAnimal(@PathParam("animal_id") String animal_id) throws JAXBException {
    	// Set Cage and Animal at null
    	Cage cage = null;
    	Animal animal = null;
    	// We see for every cages
    	for(Cage c : center.getCages()) {
    		// We see for every animal in cages
    		for(Animal a : c.getResidents()) {
    			// Animal_id is same at the animal we found
    			if (a.getId().equals(UUID.fromString(animal_id))) {
    				cage = c;
    				animal = a;
    			}
    		}
    	}
    	// Delete the specified animal
    	if(cage != null && animal != null) {
    		cage.getResidents().remove(animal);
    	} else {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}
    	//Envoi du centre sur la BDD
        uploadCenter();
    }
    
    /**
     * PUT Method bound to calls on /animals/{animal_id}
     */
    @PUT
    @Path("/animals/{animal_id}")
    @Consumes("application/xml")
    @Produces("application/xml")
    public void putAnimal(Animal newAnimal, @PathParam("animal_id") String animal_id) throws JAXBException {
    	// Set Cage and Animal at null
    	Cage cage = null;
    	Animal animal = null;
    	// We see for every cages
    	for(Cage c : center.getCages()) {
    		// We see for every animal in cages
    		for(Animal a : c.getResidents()) {
    			// Animal_id is same at the animal we found
    			if (a.getId().equals(UUID.fromString(animal_id))) {
    				cage = c;
    				animal = a;
    			}
    		}
    	}
    	// Delete the old animal and add the new one
    	if(cage != null && animal != null) {
    		cage.getResidents().remove(animal);
    		cage.getResidents().add(newAnimal);
    	}
    	//Envoi du centre sur la BDD
        uploadCenter();
    }
    
    /**
     * POST Method bound to calls on /animals
     */
    @POST
    @Path("/animals/")
    @Consumes("application/xml")
    @Produces("application/xml")
    public void postAnimal(Animal animal) throws JAXBException {
        //Add animal to his cage if she exist
        this.center.getCages()
                .stream()
                .filter(cage -> cage.getName().equals(animal.getCage()))
                .findFirst()
                .orElseThrow(() -> new WebApplicationException(Response.Status.NOT_FOUND))
                .getResidents()
                .add(animal);
        //Envoi du centre sur la BDD
        uploadCenter();
    }
    
    /**
     * PUT Method bound to calls on /animals
     */
    @PUT
    @Path("/animals/")
    @Consumes("application/xml")
    @Produces("application/xml")
    public void putAnimal(Animal animal) throws JAXBException {
    	//Clear all cages
    	for( Cage c : center.getCages() ) {
    		c.getResidents().clear();
    	}
    	//Add the animal
    	this.center.getCages()
	            .stream()
	            .filter(cage -> cage.getName().equals(animal.getCage()))
	            .findFirst()
	            .orElseThrow(() -> new WebApplicationException(Response.Status.NOT_FOUND))
	            .getResidents()
	            .add(animal);
    	//Envoi du centre sur la BDD
        uploadCenter();
    }
    
    /**
     * DELETE Method bound to calls on /animals
     */
    @DELETE
    @Path("/animals")
    @Produces("application/xml")
    public void deleteAnimal() throws JAXBException {
    	//Clear all cages
    	for( Cage c : center.getCages() ) {
    		c.getResidents().clear();
    	}
    	//Envoi du centre sur la BDD
        uploadCenter();
    }
}
