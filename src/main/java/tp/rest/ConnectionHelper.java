package tp.rest;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

import com.cloudant.client.api.ClientBuilder;
import com.cloudant.client.api.CloudantClient;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ConnectionHelper {
	static String VCAP_SERVICES = System.getenv("VCAP_SERVICES");

	public static CloudantClient createClient() {
		String url;
		if (VCAP_SERVICES != null) {
			// If running in Bluemix
			JsonObject cloudantCredentials = ConnectionHelper.getCloudCredentials("cloudant");
			if(cloudantCredentials == null){
				return null;
			}
			url = cloudantCredentials.get("url").getAsString();
		} else {
			// If running in local
			url = ConnectionHelper.getLocalProperties("cloudant.properties").getProperty("cloudant_url");
			if(url == null || url.length() == 0){
				return null;
			}
		}

		try {
			return ClientBuilder.url(new URL(url)).build();
		} catch (Exception e) {
			return null;
		}
	}
	
	private static JsonObject getCloudCredentials(String serviceName) {
		if(VCAP_SERVICES == null){
			return null;
		}
		//Convert VCAP_SERVICES String to JSON
		JsonObject obj = (JsonObject) new JsonParser().parse(VCAP_SERVICES);		
		Entry<String, JsonElement> dbEntry = null;
		Set<Entry<String, JsonElement>> entries = obj.entrySet();
		
		// Look for the VCAP key that holds the service info
		for (Entry<String, JsonElement> eachEntry : entries) {
			if (eachEntry.getKey().toLowerCase().contains(serviceName)) {
				dbEntry = eachEntry;
				break;
			}
		}
		if (dbEntry == null) {
			return null;
		}

		obj = (JsonObject) ((JsonArray) dbEntry.getValue()).get(0);

		return (JsonObject) obj.get("credentials");
	}
	
	private static Properties getLocalProperties(String fileName){
		Properties properties = new Properties();
		InputStream inputStream = ConnectionHelper.class.getClassLoader().getResourceAsStream(fileName);
		try {
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties;
	}
}