package client;


import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;

import com.sun.xml.internal.ws.api.message.Packet;

import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

public class MyClient {
    private Service service;
    private JAXBContext jc;

    private static final QName qname = new QName("https://zoo-manager-hazard-chassat.eu-gb.mybluemix.net/zoo-manager", "");
    private static final String url = "https://zoo-manager-hazard-chassat.eu-gb.mybluemix.net/zoo-manager";

    public MyClient() {
        try {
            jc = JAXBContext.newInstance(Center.class, Cage.class, Animal.class, Position.class);
        } catch (JAXBException je) {
            System.out.println("Cannot create JAXBContext " + je);
        }
    }
    
    /**
     * Adding a cage
     * @param animal
     * @throws JAXBException
     */
    public void add_cage(Cage cage) throws JAXBException {
        System.out.println("-- Ajout d'une cage : "+cage.getName());

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/cage");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "POST");
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/xml");
        requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, requestHeaders);
        try {
        	Source result = dispatcher.invoke(new JAXBSource(jc, cage));
            printSource(result);
        } catch(Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }
    
    /**
     * Delete a cage with his name
     * @param id
     * @throws JAXBException
     */
    public void delete_cage_with_name(String name) throws JAXBException {
        System.out.println("-- Suppression de la cage avec son nom="+name);
    	
        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/cage/" + name);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "DELETE");
        requestContext.put("Content-Type", "application/xml;charset=\"utf-8\"");
        try {
	        Source result = dispatcher.invoke(null);
	        printSource(result);
        } catch(Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }
    
    /**
     * Adding an animal
     * @param animal
     * @throws JAXBException
     */
    public void add_animal(Animal animal) throws JAXBException {
        System.out.println("-- Ajout d'un animal : "+animal.toString());

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "POST");
        Map<String, Object> requestHeaders = new HashMap<String, Object>();
        requestHeaders.put("Content-Type", "application/xml");
        requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, requestHeaders);
        try {
	        Source result = dispatcher.invoke(new JAXBSource(jc, animal));
	        printSource(result);
        } catch(Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }
    
    /**
     * Add an animal with his ID
     * @param animal
     * @throws JAXBException
     */
    public void add_animal_with_id(Animal animal, UUID id) throws JAXBException {
        System.out.println("-- Ajout d'un animal avec son id="+id+" : "+animal.toString());

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals/" + id);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "POST");
        requestContext.put("Content-Type", "application/xml");
        try {
	        Source result = dispatcher.invoke(new JAXBSource(jc, animal));
	        printSource(result);
        } catch(Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }
    
    /**
     * Edit an animal with his ID
     * @param animal
     * @param id
     * @throws JAXBException
     */
    public void edit_animal_with_id(Animal animal, UUID id) throws JAXBException {
        System.out.println("-- Edition d'un animal avec son id="+id+" : "+animal.toString());

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals/" + id);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "PUT");
        requestContext.put("Content-Type", "application/xml");
        try {
	        Source result = dispatcher.invoke(new JAXBSource(jc, animal));
	        printSource(result);
        } catch(Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }
    
    /**
     * Delete an animal with his ID
     * @param id
     * @throws JAXBException
     */
    public void delete_animal_with_id(UUID id) throws JAXBException {
        System.out.println("-- Suppression d'un animal avec son id="+id.toString());

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals/" + id.toString());
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "DELETE");
        requestContext.put("Content-Type", "application/xml");
        try {
	        Source result = dispatcher.invoke(null);
	        printSource(result);
        } catch(Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }
    
    /**
     * Clear all cages and add animal to his cage
     * @param animal
     * @throws JAXBException
     */
    public void edit_all_animals(Animal animal) throws JAXBException {
        System.out.println("-- Edition des animaux : "+animal.toString());

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "PUT");
        requestContext.put("Content-Type", "application/xml");
        try {
	        Source result = dispatcher.invoke(new JAXBSource(jc, animal));
	        printSource(result);
        } catch(Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }
    
    /**
     * Deleting all animals
     * @throws JAXBException
     */
    public void delete_animals() throws JAXBException {
        System.out.println("-- Suppression de tous les animaux");

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "DELETE");
        requestContext.put("Content-Type", "application/xml");
        try {
	        Source result = dispatcher.invoke(null);
	        printSource(result);
        } catch (Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }
    
    /**
     * Deleting all animals in cage c
     * @throws JAXBException
     */
    public void delete_animals(String cage_name) throws JAXBException {
        System.out.println("-- Suppression de tous les animaux se trouvant dans la cage "+cage_name);

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/cage/" + cage_name + "/animals");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "DELETE");
        requestContext.put("Content-Type", "application/xml");
        try {
	        Source result = dispatcher.invoke(null);
	        printSource(result);
        } catch(Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }

    /**
     * Showing all animals
     * @throws JAXBException
     */
    public void show_animals() throws JAXBException {
        System.out.println("-- Afficher les animaux");

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
        requestContext.put("Content-Type", "application/xml");
        try {
	        Source result = dispatcher.invoke(null);
	        printSource(result);
        } catch(Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }
    
    /**
     * Showing animals with his id
     * @throws JAXBException
     */
    public void show_animals(UUID id) throws JAXBException {
        System.out.println("-- Afficher l'animal d'id="+id.toString());

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals/" + id.toString());
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
        requestContext.put("Content-Type", "application/xml");
        try {
	        Source result = dispatcher.invoke(null);
	        printSource(result);
        } catch(Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }
    
    /**
     * Show a cage
     * @param animal
     * @throws JAXBException
     */
    public void show_cage(String cage_name) throws JAXBException {
        System.out.println("-- Afficher la cage identifi�e par le nom="+cage_name);

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/cage/" + cage_name);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "POST");
        requestContext.put("Content-Type", "application/xml");
        try {
	        Source result = dispatcher.invoke(null);
	        printSource(result);
        } catch(Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }
    
    /**
     * Search animal by name
     * @throws JAXBException
     */
    public void findByName(String name) throws JAXBException {
        System.out.println("-- Recherche d'un animal par son nom="+name);

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/find/byName/" + name);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
        requestContext.put("Content-Type", "application/xml");
        try {
	        Source result = dispatcher.invoke(null);
	        printSource(result);
        } catch(Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }
    
    /**
     * Search animal at a position
     * @throws JAXBException
     * @throws MalformedURLException 
     * @throws UnsupportedEncodingException 
     */
    public void findAt(Position pos) throws JAXBException, UnsupportedEncodingException {
        System.out.println("-- Recherche d'un animal � la position="+pos.toString());

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/find/at/" + URLEncoder.encode(pos.toString(), "UTF-8") );
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
        requestContext.put("Content-Type", "application/xml");
        try {
	        Source result = dispatcher.invoke(null);
	        printSource(result);
	    } catch(Exception e) {}
        System.out.println(requestContext.toString());
	    Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
		System.out.println("Code de retour : "+responseCode);
    }

    /**
     * Search animal near a position
     * @throws JAXBException
     * @throws UnsupportedEncodingException 
     * @throws MalformedURLException 
     */
    public void findNear(Position pos) throws JAXBException, UnsupportedEncodingException {
        System.out.println("-- Recherche d'un animal proche de la position="+pos.toString());

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/find/near/" + URLEncoder.encode(pos.toString(), "UTF-8") );
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
        requestContext.put("Content-Type", "application/xml");
        try {
	        Source result = dispatcher.invoke(null);
	        printSource(result);
        } catch(Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }
    
    /**
     * Get wolfram info from id
     * @throws JAXBException
     */
    public void wolframInfo(UUID id) throws JAXBException {
        System.out.println("-- Affichage des information Wolfram Alpha � partir d'un id="+id.toString());

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals/" + id.toString() + "/wolf");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
        requestContext.put("Content-Type", "application/xml");
        try {
	        Source result = dispatcher.invoke(null);
	        printSource(result);
        } catch(Exception e) {}
        System.out.println(requestContext.toString());
        Integer responseCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
    	System.out.println("Code de retour : "+responseCode);
    }
    
    /**
     * Show Source on output
     * @param s
     */
    public void printSource(Source s) {
        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            transformer.transform(s, new StreamResult(System.out));
            System.out.println();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Main method
     * @param args
     * @throws Exception
     */
    public static void main(String args[]) throws Exception {
        MyClient client = new MyClient();
        client.show_animals();
        client.delete_animals();
        client.show_animals();
        client.add_cage(new Cage("Rouen", new Position(49.443889d, 1.103333d), 50, new LinkedList<>()));
        client.add_animal(new Animal("nom", "Rouen", "Panda", UUID.randomUUID()));
        client.add_cage(new Cage("Paris", new Position(48.856578d, 2.351828d), 50, new LinkedList<>()));
        client.add_animal(new Animal("nom", "Paris", "Hocco unicorne", UUID.randomUUID()));
        client.show_animals();
        client.edit_all_animals(new Animal("nom","Rouen","Lagotriche � queue jaune", UUID.randomUUID()));
        client.show_animals();
        client.add_cage(new Cage("Somalie", new Position(2.333333d, 48.85d), 50, new LinkedList<>()));
        client.add_animal(new Animal("nom", "Somalie", "Oc�anite�de�Matsudaira", UUID.randomUUID()));
		client.add_animal(new Animal("nom", "Paris", "Tuit�tuit", UUID.randomUUID()));
        client.add_cage(new Cage("Canada", new Position(43.2d, -80.38333d), 50, new LinkedList<>()));
		Animal saiga = new Animal("nom", "Canada", "Sa�ga", UUID.randomUUID());
		client.add_animal(saiga);
        client.add_cage(new Cage("Bihorel", new Position(49.455278d, 1.116944d), 50, new LinkedList<>()));
		Animal galado_de_rondo = new Animal("nom", "Bihorel", "Galago�de�Rondo", UUID.randomUUID());
		client.add_animal(galado_de_rondo);
		Animal ara_de_spix = new Animal("nom", "Rouen", "Ara�de�Spix", UUID.randomUUID());
		client.add_animal(ara_de_spix);
        client.add_cage(new Cage("Londres", new Position(51.504872d, -0.07857d), 50, new LinkedList<>()));
		client.add_animal(new Animal("nom", "Londres", "Palette�des�Sulu", UUID.randomUUID()));
		client.add_animal(new Animal("nom", "Paris", "Kouprey", UUID.randomUUID()));
        client.add_cage(new Cage("Porto�Vecchio", new Position(41.5895241d, 9.2627d), 50, new LinkedList<>()));
		client.add_animal(new Animal("nom", "Porto�Vecchio", "Inca�de�Bonaparte", UUID.randomUUID()));
		client.show_animals();
        client.add_cage(new Cage("Villers�Bocage", new Position(50.0218d, 2.3261d), 50, new LinkedList<>()));
		client.add_animal(new Animal("nom", "Villers�Bocage", "Rhinoc�ros�de�Java", UUID.randomUUID()));
        client.add_cage(new Cage("Montreux", new Position(46.4307133d, 6.9113575d), 50, new LinkedList<>()));
		client.add_animal(new Animal("nom", "Montreux", "R�le�de�Zapata", UUID.randomUUID()));
		//Position de New York trouv�e sur le site www.coordonnees-gps.fr
        client.add_cage(new Cage("USA", new Position(48.862725d, 2.287592d), 50, new LinkedList<>()));
		for(int i=0; i<101; i++) {
			client.add_animal(new Animal("nom", "USA", "Dalmatiens", UUID.randomUUID()));
		}
		client.show_animals();
		client.delete_animals("Paris");
		client.show_animals();
		client.show_animals(ara_de_spix.getId());
		client.delete_animal_with_id(ara_de_spix.getId());
		client.delete_animal_with_id(ara_de_spix.getId());
		client.show_animals();
		//Position de Rouen trouv�e sur le site www.coordonnees-gps.fr
		client.findNear(new Position(49.443232d, 1.099971d));
		client.findAt(new Position(49.443232d, 1.099971d));
		client.wolframInfo(saiga.getId());
		client.wolframInfo(galado_de_rondo.getId());
		//TODO
		//TODO
		client.delete_animals();
		client.show_animals();		
    }
}
