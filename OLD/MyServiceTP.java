package tp.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Provider;
import javax.xml.ws.Service;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;
import javax.xml.ws.http.HTTPException;

import tp.model.Animal;
import tp.model.AnimalNotFoundException;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

@WebServiceProvider
@ServiceMode(value = Service.Mode.MESSAGE)
public class MyServiceTP implements Provider<Source> {

    public final static String url = "http://127.0.0.1:8084/";

    /**
     * Main function
     * @param args
     */
    public static void main(String args[]) {
        Endpoint e = Endpoint.create(HTTPBinding.HTTP_BINDING, new MyServiceTP());

        e.publish(url);
        System.out.println("Service started, listening on " + url);
        // pour arrêter : e.stop();
    }

    private JAXBContext jc;

    @javax.annotation.Resource(type = Object.class)
    protected WebServiceContext wsContext;

    private Center center = new Center(new LinkedList<>(), new Position(49.30494d, 1.2170602d), "Biotropica");

    /**
     * Main constructor
     */
    public MyServiceTP() {
        try {
            jc = JAXBContext.newInstance(Center.class, Cage.class, Animal.class, Position.class);
        } catch (JAXBException je) {
            System.out.println("Exception " + je);
            throw new WebServiceException("Cannot create JAXBContext", je);
        }

        // Fill our center with some animals
        /*
        Cage usa = new Cage(
                "usa",
                new Position(49.305d, 1.2157357d),
                25,
                new LinkedList<>(Arrays.asList(
                        new Animal("Tic", "usa", "Chipmunk", UUID.randomUUID()),
                        new Animal("Tac", "usa", "Chipmunk", UUID.randomUUID())
                ))
        );

        Cage amazon = new Cage(
                "amazon",
                new Position(49.305142d, 1.2154067d),
                15,
                new LinkedList<>(Arrays.asList(
                        new Animal("Canine", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Incisive", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Molaire", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("De lait", "amazon", "Piranha", UUID.randomUUID())
                ))
        );

        center.getCages().addAll(Arrays.asList(usa, amazon));*/
    }

    /**
     * Method called when request is recieved
     */
    public Source invoke(Source source) {
        MessageContext mc = wsContext.getMessageContext();
        String path = (String) mc.get(MessageContext.PATH_INFO);
        String method = (String) mc.get(MessageContext.HTTP_REQUEST_METHOD);

        // determine the targeted ressource of the call
        try {
            // no target, throw a 404 exception.
            if (path == null) {
                throw new HTTPException(404);
            }
            // "/animals" target - Redirect to the method in charge of managing this sort of call.
            else if (path.startsWith("animals")) {
                String[] path_parts = path.split("/");
                switch (path_parts.length){
                    case 1 :
                    	// "/animals" POST, GET, PUT and DELETE
                        return this.animalsCrud(method, source);
                    case 2 :
                    	// "/animals/{animal_id}" POST, GET, PUT and DELETE
                        return this.animalCrud(method, source, path_parts[1]);
                    case 3 :
                    	// "/animals/{animal_id}/wolf" POST
                    	if(path_parts[2].equals("wolf")) return this.wolframInfo(method, source, path_parts[1]);
                    	else throw new HTTPException(404);
                    default:
                        throw new HTTPException(404);
                }
            }
         // "/animals" target - Redirect to the method in charge of managing this sort of call.
            else if (path.startsWith("cage")) {
                String[] path_parts = path.split("/");
                switch (path_parts.length){
                    case 1 :
                    	// "/cage" POST
                        return this.cageCrud(method, source);
                    case 2 :
                    	// "/cage/{name}" DELETE
                        return this.cageCrud(method, source, path_parts[1]);
                    case 3 :
                    	// "/cage/{name}/animals" DELETE
                    	if(path_parts[2].equals("animals")) return this.cageAnimalsCrud(method, source, path_parts[1]);
                    	else throw new HTTPException(404);
                    default:
                        throw new HTTPException(404);
                }
            }
            // "/find" target
            else if (path.startsWith("find/")) {
                String[] path_parts = path.split("/");
                // If there isn't 3 part in the path throw 404 ERROR
                if( path_parts.length != 3) throw new HTTPException(404);
                switch(path_parts[1]) {
		            case "byName" : 
		                // "/find/byName/{name}" GET Recherche d'un animal par son nom
	            		return this.findByName(method, source, path_parts[2]);
		            case "at" : 
		            	// "/find/at/{position}" GET Recherche d'un animal par position
		            	return this.findAt(method, source, path_parts[2]);
		            case "near" : 
		            	// "/find/near/{position}" GET Recherche des animaux près d’une position
		            	return this.findNear(method, source, path_parts[2]);
                	default : throw new HTTPException(404);
                }
            }
            else if ("coffee".equals(path)) {
                throw new HTTPException(418);
            }
            else {
                throw new HTTPException(404);
            }
        } catch (JAXBException e) {
            throw new HTTPException(500);
        }
    }

    /**
     * Method bound to calls on /animals/{animal_id}/wolf
     */
    private Source wolframInfo(String method, Source source, String animal_id) throws JAXBException {
		if("GET".equals(method)){
			try {
				//Search animal who is identified by the animal_id
				Animal animal = center.findAnimalById(UUID.fromString(animal_id));
				//Get info from wolfram
				String wolfInfo = requestHttpGetOn("https://api.wolframalpha.com/v2/query?input="+animal.getSpecies()+"&output=XML&appid=JA52GU-5GE7642L3K");
				//Return info as String (XML because of "output=XML" in the request )
				return new StreamSource(new StringReader(wolfInfo));
            } catch (AnimalNotFoundException e) {
                throw new HTTPException(404);
            }
        }
        else{
            throw new HTTPException(405);
        }
	}
    
    /**
     * Method who do a GET HTTP request on uri
     * @param uri
     * @return result of the request
     */
    private String requestHttpGetOn(String uri) {
		try { 
		    //Create an HttpUrlConnection (GET)
			URL url = new URL(uri);
		    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		    connection.setDoOutput(true);
		    connection.setRequestMethod("GET");
		    connection.setRequestProperty("Content-Type", "application/xml");
		    
		    //If response code is HTTP_OK (200)
		    if(connection.getResponseCode() == 200) {
		    	//Get content of the response
		        StringBuilder result = new StringBuilder();
		    	BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		        String line;
		        while ((line = rd.readLine()) != null) {
		           result.append(line);
		        }
		        rd.close();
			    connection.disconnect();
			    //Return response as String
		        return result.toString();
		    } else {
		    	//Return null if response code isn't HTTP_OK (200)
			    connection.disconnect();
		    	return null;
		    }
		} catch(Exception e) {
		    throw new RuntimeException(e);
		}
    }
    
    /**
     * Method bound to calls on /cage
     */
	private Source cageCrud(String method, Source source) throws JAXBException {
		if("POST".equals(method)){
			//Decode source as Cage
            Cage c = unmarshalCage(source);
            //Verify if cage already exist (search by name)
            for(Cage cage : center.getCages()) {
            	if(c.getName().equals(cage.getName())) {
            		//If yes => Error HTTP 409 : Conflict
            		throw new HTTPException(409); 
            	}
            }
            //If not found => add the cage and return the cage
            this.center.getCages().add(c);
            return new StreamSource();
        }
        else{
            throw new HTTPException(405);
        }
	}
	
	/**
     * Method bound to calls on /cage/{name}
     */
	private Source cageCrud(String method, Source source, String name) throws JAXBException {
		if("DELETE".equals(method)){
			Cage cage = null;
			//Search the cage with his {name}
			for(Cage c : center.getCages()) {
				if(c.getName().equals(name)) {
					cage = c;
				}
			}
			//If found remove the cage
			if(cage != null) {
				center.getCages().remove(cage);
	            return new StreamSource();
			} else {
				//If not return HTTP 404 error  
				 throw new HTTPException(404);
			}
        }
        else{
            throw new HTTPException(405);
        }
	}
	
	/**
     * Method bound to calls on /cage/{name}/animal
     */
	private Source cageAnimalsCrud(String method, Source source, String name) throws JAXBException {
		if("DELETE".equals(method)){
			Cage cage = null;
			//Search the cage with his {name}
			for(Cage c : center.getCages()) {
				if(c.getName().equals(name)) {
					cage = c;
				}
			}
			//If found remove all animals in the cage
			if(cage != null) {
				cage.getResidents().clear();
	            return new StreamSource();
			} else {
				//If not return HTTP 404 error  
				throw new HTTPException(404);
			}
        }
        else{
            throw new HTTPException(405);
        }
	}

	/**
     * Method bound to calls on /find/byName/{name}
     */
    private Source findByName(String method, Source source, String name) throws JAXBException {
    	if("GET".equals(method)){
    		//Search in all cage, all residents
    		for( Cage c : this.center.getCages() ) {
    			for( Animal a : c.getResidents() ) {
    				//Return the animal who have the same name as {name}
    				if( a.getName().equals(name) ) {
    		    		return new JAXBSource(this.jc, a);
    				}
    			}
    		}
    		//If no animal found 404 ERROR
    		throw new HTTPException(404);
        }
        else{
        	//If it's not a GET method, 405 ERROR
            throw new HTTPException(405);
        }
	}
    
    /**
     * Method bound to calls on /find/at/{position}
     */
    private Source findAt(String method, Source source, String position) throws JAXBException {
    	if("GET".equals(method)){
    		//Search in all cage who is at the {position}
    		for( Cage c : this.center.getCages() ) {
    			if( c.getPosition().toString().equals(position) && !c.getResidents().isEmpty()) {
    				//Return the first animal of the resident list
        		    return new JAXBSource(this.jc, c.getResidents().toArray()[0]);
    			}
    		}
    		//If no animal found 404 ERROR
    		throw new HTTPException(404);
        }
        else{
        	//If it's not a GET method, 405 ERROR
        	throw new HTTPException(405);
        }
	}
    
    /**
     * Method bound to calls on /find/near/{position}
     */
    private Source findNear(String method, Source source, String position) throws JAXBException {
    	if("GET".equals(method)){
    		//Parse the position
    		String[] posStr = position.substring(1, position.length()-1).replace(" ", "").split(",");
    		Double latitude = Double.parseDouble(posStr[0]);
    		Double longitude = Double.parseDouble(posStr[1]);
    		Position pos = new Position(latitude,longitude);
    		
    		//Search in all cage who is near the {position}
    		Cage cagefound = this.near(pos);
			if( cagefound != null && !cagefound.getResidents().isEmpty()) {
				//Return the resident list of the cage found
    		    return new JAXBSource(this.jc, cagefound.getResidents().toArray());
			}
    		//If no cage found 404 ERROR
    		throw new HTTPException(404);
        }
        else{
        	//If it's not a GET method, 405 ERROR
        	throw new HTTPException(405);
        }
	}

    /**
     * Method who return the cage who is near to p
     * @param p
     * @return cage near to p
     */
    private Cage near(Position p) {
    	Cage cFound = null;
    	double dist = 150000;
    	//Search a cage who in near (150km) of the position p
    	for(Cage c : center.getCages()) {
    		if(getDistance(c.getPosition(),p) <= dist) {
        		cFound = c;
    		}
    	}
    	return cFound;
    }
    
    /**
     * Return the distance between position a and b
     * @param a
     * @param b
     * @return distance between position a and b
     */
    private double getDistance(Position a, Position b) {
    	//--- Begin of Hard math ---//
        float pk = (float) (180.f/Math.PI);
       
        double a1 = a.getLatitude() / pk;
        double a2 = a.getLongitude() / pk;
        double b1 = b.getLatitude() / pk;
        double b2 = b.getLongitude() / pk;
       
        double d1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double d2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double d3 = Math.sin(a1) * Math.sin(b1);
        double dist = Math.acos(d1 + d2 + d3);
        //--- End of Hard math ---//
        return (6366000 * dist);
    }
    
	/**
     * Method bound to calls on /animals/{animal_id}
     */
    private Source animalCrud(String method, Source source, String animal_id) throws JAXBException {
        // Using the GET Method
        if("GET".equals(method)){
            try {
            	//Return animal identified by {animal_id}
                return new JAXBSource(this.jc, center.findAnimalById(UUID.fromString(animal_id)));
            } catch (AnimalNotFoundException e) {
                throw new HTTPException(404);
            }
        }
        // Using the POST Method
        else if("POST".equals(method)){
        	//Get the animal with the source 
        	Animal animal = unmarshalAnimal(source);
        	// Set the id of the animal which will be add 
        	animal.setId(UUID.fromString(animal_id));
            this.center.getCages()
                    .stream()
                    .filter(cage -> cage.getName().equals(animal.getCage()))
                    .findFirst()
                    .orElseThrow(() -> new HTTPException(404))
                    .getResidents()
                    .add(animal);
            return new StreamSource();
        }
        // Using the DELETE Method
        else if("DELETE".equals(method)){
        	// Set Cage and Animal at null
        	Cage cage = null;
        	Animal animal = null;
        	// We see for every cages
        	for(Cage c : center.getCages()) {
        		// We see for every animal in cages
        		for(Animal a : c.getResidents()) {
        			// Animal_id is same at the animal we found
        			if (a.getId().equals(UUID.fromString(animal_id))) {
        				cage = c;
        				animal = a;
        			}
        		}
        	}
        	// Delete the specified animal
        	if(cage != null && animal != null) {
        		cage.getResidents().remove(animal);
        	} else {
        		throw new HTTPException(404);
        	}
        	//Return the center
            return new StreamSource();
        }
        // Using the PUT Method
        else if("PUT".equals(method)){
        	// Get the animal with the source 
        	Animal newAnimal = unmarshalAnimal(source);
        	// Set Cage and Animal at null
        	Cage cage = null;
        	Animal animal = null;
        	// We see for every cages
        	for(Cage c : center.getCages()) {
        		// We see for every animal in cages
        		for(Animal a : c.getResidents()) {
        			// Animal_id is same at the animal we found
        			if (a.getId().equals(UUID.fromString(animal_id))) {
        				cage = c;
        				animal = a;
        			}
        		}
        	}
        	// Delete the old animal and add the new one
        	if(cage != null && animal != null) {
        		cage.getResidents().remove(animal);
        		cage.getResidents().add(newAnimal);
        	}
        	//Return the center
            return new StreamSource();
        }
        else{
            throw new HTTPException(405);
        }
    }

    /**
     * Method bound to calls on /animals
     */
    private Source animalsCrud(String method, Source source) throws JAXBException {
    	// Using the GET Method
        if("GET".equals(method)){
        	//Return the center
            return new JAXBSource(this.jc, this.center);
        }
        // Using the POST Method
        else if("POST".equals(method)){
        	//Parse the source as Animal
            Animal animal = unmarshalAnimal(source);
            //Add animal to his cage if she exist
            this.center.getCages()
                    .stream()
                    .filter(cage -> cage.getName().equals(animal.getCage()))
                    .findFirst()
                    .orElseThrow(() -> new HTTPException(404))
                    .getResidents()
                    .add(animal);
            //Return the center
            return new StreamSource();
        }
        // Using the PUT Method
        else if("PUT".equals(method)){
        	//Clear all cages
        	for( Cage c : center.getCages() ) {
        		c.getResidents().clear();
        	}
        	//Parse the source as Animal
        	Animal animal = unmarshalAnimal(source);
        	//Add the animal
        	this.center.getCages()
		            .stream()
		            .filter(cage -> cage.getName().equals(animal.getCage()))
		            .findFirst()
		            .orElseThrow(() -> new HTTPException(404))
		            .getResidents()
		            .add(animal);
        	//Return the center
        	return new StreamSource();
        }
        // Using the DELETE Method
        else if("DELETE".equals(method)){
        	//Clear all cages
        	for( Cage c : center.getCages() ) {
        		c.getResidents().clear();
        	}
        	//Return the center
        	return new StreamSource();
        }
        else{
            throw new HTTPException(405);
        }
    }

    /**
     * Method who parse Source and return an Animal
     * @param source
     * @return
     * @throws JAXBException
     */
    private Animal unmarshalAnimal(Source source) throws JAXBException {
        return (Animal) this.jc.createUnmarshaller().unmarshal(source);
    }
    
    /**
     * Method who parse Source and return an Cage
     * @param source
     * @return
     * @throws JAXBException
     */
    private Cage unmarshalCage(Source source) throws JAXBException {
    	Cage cage = (Cage) this.jc.createUnmarshaller().unmarshal(source);
    	if(cage.getResidents() == null) {
    		cage.setResidents(new LinkedList<Animal>());
    	}
        return cage;
    }
}
