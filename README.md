# TP RESTFUL d'Architectures Distribuees

</p>
HAZARD Alexandre<br/>
ROMAIN Chassat
</p>

<table>
<tbody>
<tr>
<td width="85">
<p><strong>M&Eacute;THODE</strong></p>
</td>
<td width="151">
<p><strong>URL</strong></p>
</td>
<td width="57">
<p><strong>BODY</strong></p>
</td>
<td width="311">
<p><strong>DESCRIPTION</strong></p>
</td>
</tr>
<tr>
<td width="85">
<p>GET</p>
</td>
<td width="151">
<p>/animals</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>Retourne l'ensemble des animaux du centre</p>
</td>
</tr>
<tr>
<td width="85">
<p>GET</p>
</td>
<td width="151">
<p>/animals/{animal_id}</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>Retourne l&rsquo;animal identifi&eacute; par {animal_id}</p>
</td>
</tr>
<tr>
<td width="85">
<p>&nbsp;</p>
</td>
<td width="151">
<p>&nbsp;</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="85">
<p>PUT</p>
</td>
<td width="151">
<p>/animals</p>
</td>
<td width="57">
<p>Animal</p>
</td>
<td width="311">
<p>Modifie l'ensemble des animaux</p>
</td>
</tr>
<tr>
<td width="85">
<p>DELETE</p>
</td>
<td width="151">
<p>/animals</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>Supprime l'ensemble des animaux</p>
</td>
</tr>
<tr>
<td width="85">
<p>&nbsp;</p>
</td>
<td width="151">
<p>&nbsp;</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="85">
<p>POST</p>
</td>
<td width="151">
<p>/animals</p>
</td>
<td width="57">
<p>Animal</p>
</td>
<td width="311">
<p>Ajoute un animal dans votre centre</p>
</td>
</tr>
<tr>
<td width="85">
<p>POST</p>
</td>
<td width="151">
<p>/animals/{animal_id}</p>
</td>
<td width="57">
<p>Animal</p>
</td>
<td width="311">
<p>Cr&eacute;e l&rsquo;animal identifi&eacute; par {animal_id}</p>
</td>
</tr>
<tr>
<td width="85">
<p>PUT</p>
</td>
<td width="151">
<p>/animals/{animal_id}</p>
</td>
<td width="57">
<p>Animal</p>
</td>
<td width="311">
<p>Modifie l&rsquo;animal identifi&eacute; par {animal_id}</p>
</td>
</tr>
<tr>
<td width="85">
<p>DELETE</p>
</td>
<td width="151">
<p>/animals/{animal_id}</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>Supprime l&rsquo;animal identifi&eacute; par {animal_id}</p>
</td>
</tr>
<tr>
<td width="85">
<p>&nbsp;</p>
</td>
<td width="151">
<p>&nbsp;</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="85">
<p>GET</p>
</td>
<td width="151">
<p>/find/byName/{name}</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>Recherche d'un animal par son nom</p>
</td>
</tr>
<tr>
<td width="85">
<p>GET</p>
</td>
<td width="151">
<p>/find/at/{position}</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>Recherche d'un animal par position</p>
</td>
</tr>
<tr>
<td width="85">
<p>GET</p>
</td>
<td width="151">
<p>/find/near/{position}</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>Recherche des animaux pr&egrave;s d&rsquo;une position</p>
</td>
</tr>
<tr>
<td width="85">
<p>&nbsp;</p>
</td>
<td width="151">
<p>&nbsp;</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="85">
<p>GET</p>
</td>
<td width="151">
<p>/animals/{animal_id}/wolf</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>R&eacute;cup&eacute;ration des infos. Wolfram d&rsquo;un animal</p>
</td>
</tr>
<tr>
<td width="85">
<p>&nbsp;</p>
</td>
<td width="151">
<p>&nbsp;</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="85">
<p>POST</p>
</td>
<td width="151">
<p>/cage</p>
</td>
<td width="57">
<p>Cage</p>
</td>
<td width="311">
<p>Ajoute une cage dans votre centre</p>
</td>
</tr>
<tr>
<td width="85">
<p>DELETE</p>
</td>
<td width="151">
<p>/cage/{name}</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>Supprime la cage identifi&eacute;e par {name}</p>
</td>
</tr>
<tr>
<td width="85">
<p>DELETE</p>
</td>
<td width="151">
<p>/cage/{name}/animals</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>Supprime tous les animaux de la cage identifi&eacute;e par {name}</p>
</td>
</tr>
</tbody>
</table>
